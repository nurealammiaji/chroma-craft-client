import { Helmet } from "react-helmet-async";

const Courses = () => {
    return (
        <div className="min-h-screen">
            <Helmet>
                <title>Courses || Chroma Craft</title>
                <link rel="canonical" href="https://chromacraftbd.web.app/" />
            </Helmet>
        </div>
    );
};

export default Courses;